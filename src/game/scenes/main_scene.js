
import Scene from "~/engine/scene";
import Player from "../entities/player";
import Camera from "../entities/camera";

export default class MainScene extends Scene {
  getJsonData() { return require("./main_scene.json"); }

  beginPlay() {
    super.beginPlay();

    let player = this.spawn(Player, { x: 1, y: 1 }, 0);
    this.spawn(Camera, { x: 0, y: 0 }, 0, player);
  }
}
MainScene.registry();
