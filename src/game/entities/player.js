
import Entity from "~/engine/entities/entity";
import Physics from "~/engine/physics";
import AnimationComponent from "~/engine/components/animation_component";
import { KeyCode } from "~/engine/input";

import "~/assets/images/wall.png";


export default class Player extends Entity {
  getJsonData() { return require("./player.json"); }

  onConstruction() {
    super.onConstruction();

    this.isGoingLeft = false;

    // movement attr
    this.maxVelocity = { x: 2, y: 4 };
    this.jumpForce = 2;

    this.moving = false;
    this.moveUp = false;

    this.animation = this.findComponentOfType(AnimationComponent);
    //this.createComponent(SpriteComponent, this.root, img);
  }

  beginPlay() {
    super.beginPlay();

    setTimeout(() => {
      this.worldPosition = { x: 3, y: 2 };
    }, 5000);

    this.animation.on("fire", () => {
      this.onFire();
    });
  }

  tick(deltaTime) {
    super.tick(deltaTime);

    let input = this.world.input;
    if (input.isKeyUp(KeyCode.KEY_SPACE))
      this.stopFire();
    else if (input.isKeyDown(KeyCode.KEY_SPACE))
      this.fire();

    if (input.isKeyPressed(KeyCode.KEY_LEFT) || input.isKeyPressed(KeyCode.KEY_A))
      this.move({ x: -1, y: 0 });

    if (input.isKeyPressed(KeyCode.KEY_RIGHT) || input.isKeyPressed(KeyCode.KEY_D))
      this.move({ x: 1, y: 0 });

    if (input.isKeyPressed(KeyCode.KEY_UP) || input.isKeyPressed(KeyCode.KEY_W))
      this.jump();

    // jump
    if (this.moveUp) {
      this.applyVelocity(new Physics.Vec2(0, this.jumpForce));
      this.moveUp = false;
    }

    if (!this.firing) {
      if (this.isFalling)
        this.animation.switchAnimation("air");
      else if (this.moving)
        this.animation.switchAnimation("jump");
      else
        this.animation.switchAnimation("idle");
    }

    this.animation.scale.x = this.isGoingLeft ? -1 : 1;

    //Reset trigger values
    this.moving = false;
    //this.firing = false;
  }

  fire() {
    if (!this.firing) {
      this.firing = true;

      this.animation.switchAnimation("prepareGun", () => {
        this.animation.switchAnimation("fire");
      });
    }
  }

  onFire() {
    console.log("Bang!");

    /*fireSound.pause();
    fireSound.currentTime = 0;
    fireSound.play();*/
  }

  stopFire() {
    this.animation.switchAnimation("hideGun", () => {
      this.firing = false;
    });
  }

  move(direction) {
    if (!this.firing) {
      this.applyVelocity(new Physics.Vec2(direction.x, direction.y));
      if (direction.x > 0)
        this.isGoingLeft = false;
      else if (direction.x < 0)
        this.isGoingLeft = true;

      this.moving = direction.x != 0;
    }
  }

  jump() {
    if (this.isFalling || this.firing)
      return false;

    this.moveUp = true;
  }

  applyVelocity(vel) {
    var bodyVel = this.root.body.GetLinearVelocity();
    bodyVel.Add(vel);

    // horizontal movement cap
    if (Math.abs(bodyVel.x) > this.maxVelocity.x)
      bodyVel.x = this.maxVelocity.x * bodyVel.x / Math.abs(bodyVel.x);

    // vertical movement cap
    if (Math.abs(bodyVel.y) > this.maxVelocity.y)
      bodyVel.y = this.maxVelocity.y * bodyVel.y / Math.abs(bodyVel.y);

    this.root.body.SetLinearVelocity(bodyVel);
  }

  get isFalling() {
    return Math.abs(this.root.body.GetLinearVelocity().y) > 0.05;
  }
}
Player.registry();
