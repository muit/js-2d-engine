
import Entity from "~/engine/entities/entity";
import SpriteComponent from "~/engine/components/sprite_component";

import img from "~/assets/images/wall.png";

export default class Floor extends Entity {
  constructor(scene) {
    super(scene);
    this.root = this.createComponent(SpriteComponent, null, img);
  }
  onConstruction() {
    super.onConstruction();
  }

  beginPlay() {
    super.beginPlay();
  }
}
Floor.registry();
