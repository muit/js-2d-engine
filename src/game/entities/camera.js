
import Entity from "~/engine/entities/entity";
import CameraComponent from "~/engine/components/camera_component";
import Physics from "~/engine/physics";


export default class Camera extends Entity {
  constructor(...args) {
    super(...args);
    // Create components and default variables here. Always call super.

    this.root = this.createComponent(CameraComponent);
  }

  onConstruction(player) {
    // Receive parameters here, called just before begin play
    super.onConstruction();
    this.player = player;
  }

  beginPlay() {
    // Called when the game starts for this entity
    super.beginPlay();

    this.offset = { x: 0, y: 0 };
    this.minX = -800;
    this.maxX = 800;
    this.minY = -800;
    this.speed = 2;
  }

  tick(deltaTime) {
    // This camera follows the player's position
    if (this.player) {
      const currentPosition = this.worldPosition;

      // Calculate target
      let desiredPosition = {
        x: Math.clamp(this.player.worldPosition.x - this.offset.x, this.minX, this.maxX),
        y: this.player.worldPosition.y - this.offset.y
      };

      // Smooth movement
      desiredPosition.x = Math.lerp(currentPosition.x, desiredPosition.x, deltaTime * this.speed);
      desiredPosition.y = Math.lerp(currentPosition.y, desiredPosition.y, deltaTime * this.speed);

      // Assign new position
      this.worldPosition = desiredPosition;
    }
  }

  render(ctx) {
    ctx.save();

    const position = this.worldPosition;
    ctx.translate(position.x * Physics.scale, -position.y * Physics.scale);

    ctx.scale(1, 1);

    // Debug camera center
    /*ctx.beginPath();
    ctx.rect(-5, -5, 10, 10);
    ctx.fillStyle = "blue";
    ctx.fill();*/

    ctx.restore();
  }
}
Camera.registry();
