
/**
 * Import all gameplay classes here
 * If a class was not loaded, related json data wont work either
 */
import "~/engine/components/animation_component";
import "~/engine/components/boxcollision_component";

import "./scenes/main_scene";
import "./entities/player";
import "./entities/floor";
