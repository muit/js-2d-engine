"use strict";

require("./index.html");
import World from "./engine/world";

var canvas = document.getElementById("canvas");

var world = new World(canvas);
world.loadScene("mainscene");
