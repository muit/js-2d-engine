
import Class from "./reflection";
import Entity from "./entities/entity";

//const scene_data = require("./data/scene.json");

export default class Scene extends Class {
  constructor(world) {
    super();
    this.__world = world;
    this.entities = [];
    this.hasBegunPlay = false;
  }

  /** Setup all entities. */
  load() {
    let data = this.getJsonData();

    // Create all entities
    if (typeof (data) == "object") {
      if (data.entities) {
        for (let entity of data.entities) {
          this.spawn(entity.type, entity.position, entity.angle, entity.parameters);
        }
      }
    }
  }

  /** Game started */
  beginPlay() {
    this.hasBegunPlay = true;

    this.iterateEntities(entity => entity.beginPlay());
  }

  tick(deltaTime) {
    this.iterateEntities(entity => entity.tick(deltaTime));
  } // eslint-disable-line no-unused-vars

  render(ctx) {
    this.iterateEntities(entity => entity.render(ctx));
  }


  getJsonData() {
    return null;
  }

  /**
   * Execute a callback for each component of this entity
   * @param {function(Component)} callback to call
   */
  iterateEntities(callback) {
    if (typeof (callback) != "function")
      return;

    for (let entity of this.entities) {
      callback(entity);
    }
  }

  /**
   * Creates a component on this entity
   * @param {Component} Type of the component
   * @param {Vector2} position to spawn at
   * @param {number} angle to spawn with
   * @param {*} args that will go into "onConstruction(args)"
   * @return {Component} created component
   */
  spawn(Type, position, angle, ...args) {
    if (typeof (Type) === "string") {
      Type = Class.findClass(Type);
    }

    if (Type && Type.isChildOf(Entity)) {
      return Type.spawn(this, position, angle, ...args);
    }
    return null;
  }

  get world() { return this.__world; }
}
Scene.registry();
