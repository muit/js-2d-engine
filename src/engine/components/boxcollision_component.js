
import SceneComponent from "./scene_component";
import { deg2Rad, Vector2 } from "../math";


export default class BoxCollisionComponent extends SceneComponent {
  onConstruction(size, mobility, config) {
    super.onConstruction();

    this.mobility = mobility != null ? mobility : SceneComponent.Mobility.KINEMATIC;

    this.size = new Vector2(size);
    this.config = config;
  }

  beginPlay() {
    this.body = this.world.physics.createBox(this,
      this.worldPosition.x, this.worldPosition.y,
      this.size.x, this.size.y, Object.assign({
        density: 5,
        fixedRotation: true,
        type: this.__physicsMode // will pick physics mode based on mobility
      }, this.config)
    );

    this.body.SetAngle(deg2Rad(this.worldAngle));
  }
}
BoxCollisionComponent.registry();
