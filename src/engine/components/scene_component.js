
import Component from "./component";
import Entity from "../entities/entity";
import Physics from "../physics.js";
import { Transform, rad2Deg } from "../math";

const Mobility = Object.freeze({
  MOVABLE: 0,   // Simulates physics
  KINEMATIC: 1, // Kinematic, moves whole actor
  STATIC: 2     // Static physics and position
});

export default class SceneComponent extends Component {

  constructor(owner) {
    super(owner);
    this.transform = new Transform();

    this.mobility = Mobility.KINEMATIC;
  }

  onConstruction() {}

  tick(deltaTime) {
    if (this.body && this.mobility == Mobility.MOVABLE) {
      // Sync position and angle from physics
      this.worldPosition = this.body.GetPosition();
      this.worldAngle = rad2Deg(this.body.GetAngle());
    }
  }

  render(ctx) {}

  attach(parent) {
    if (parent instanceof SceneComponent) {
      this.__parent = parent;
      this.transform.parent = parent.transform;
    }
  }

  get parent() {
    return (this.__parent instanceof SceneComponent) ? this.__parent : null;
  }

  /**
   * Mobility uses minimum available mobility coming from parent.
   * If this component is static but parent is kinematic, it will react as kinematic
   */
  get mobility() {
    let parent = this.parent;
    if (parent) {
      return Math.min(this.__mobility, this.parent.mobility);
    }
    return this.__mobility;
  }

  set mobility(mobility) {
    let success = true;
    switch (mobility) {
    case Mobility.MOVABLE:
      this.__physicsMode = Physics.Body.b2_dynamicBody;
      break;
    case Mobility.KINEMATIC:
      this.__physicsMode = Physics.Body.b2_kinematicBody;
      break;
    case Mobility.STATIC:
      this.__physicsMode = Physics.Body.b2_staticBody;
      break;
    default:
      success = false;
    }

    if (success) {
      this.__mobility = mobility;
      if (this.body && this.body.SetType) {
        this.body.SetType(this.__physicsMode);
      }
    }
  }

  get localPosition() { return this.transform.localPosition; }
  get worldPosition() { return this.transform.worldPosition; }
  get localAngle() { return this.transform.localAngle; }
  get worldAngle() { return this.transform.worldAngle; }

  set localPosition(position) { this.transform.localPosition = position; }
  set worldPosition(position) { this.transform.worldPosition = position; }
  set localAngle(angle) { this.transform.localAngle = angle; }
  set worldAngle(angle) { this.transform.worldAngle = angle; }

  static create(owner, parent, ...args) {
    if (Entity.isValid(owner)) {
      let component = new this(owner);

      // registry this component on the entity
      owner.components.push(component);

      component.attach(parent);
      component.onConstruction(...args);
      return component;
    }
    return null;
  }
}
SceneComponent.registry();

SceneComponent.Mobility = Mobility;
