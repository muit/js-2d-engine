
import SceneComponent from "./scene_component";
import Physics from "../physics.js";

import { deg2Rad, rad2Deg } from "../math";


export default class SpriteComponent extends SceneComponent {
  onConstruction(src, scale) {
    super.onConstruction();

    this.world.loadImage("./" + src, img => {
      this.img = img;
      this.height = this.scale * this.img.height / Physics.scale;
      this.width = this.scale * this.img.width / Physics.scale;

      this.body = this.world.physics.createBox(this,
        this.worldPosition.x, this.worldPosition.y,
        this.width, this.height, {
          density: 10,
          fixedRotation: true,
          type: this.__physicsMode // will pick physics mode based on mobility
        }
      );

      this.body.SetAngle(deg2Rad(-this.worldAngle));
    });

    this.scale = scale || 0.25;
  }

  render(ctx) {
    if (this.img) {
      ctx.save();

      const position = this.worldPosition;
      ctx.translate(position.x * Physics.scale, -position.y * Physics.scale);

      ctx.scale(this.scale, this.scale);

      ctx.rotate(deg2Rad(this.worldAngle));

      // Fix: Won't draw
      ctx.drawImage(this.img,
        -this.img.width,
        -this.img.height,
        this.img.width   * 2,
        this.img.height  * 2);

      // Debug the center of the sprite
      /*ctx.beginPath();
      ctx.rect(-5, -5, 10, 10);
      ctx.fillStyle = "red";
      ctx.fill();*/

      ctx.restore();
    }
  }
}
SpriteComponent.registry();
