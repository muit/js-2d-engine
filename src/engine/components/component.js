
import Class from "../reflection";
import Entity from "../entities/entity";

export default class Component extends Class {
  constructor(owner) {
    super();
    this.owner = owner;
  }

  onConstruction() {}

  beginPlay() {}

  tick(deltaTime) { deltaTime; }

  /** Called just after the entity gets marked for destruction */
  beginDestruction() {}

  /** Called just before the entity gets destroyed, at the end of the frame */
  finishDestruction() {}

  static create(owner, ...args) {
    if (Entity.isValid(owner)) {
      let component = new this(owner);

      // registry this component on the entity
      owner.components.push(component);

      component.onConstruction(...args);
      return component;
    }
    return null;
  }

  isValid() {
    return this.owner instanceof Entity && this.owner.isValid();
  }

  get world() { return this.owner.world; }
}
Component.registry();
