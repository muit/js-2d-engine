
import SceneComponent from "./scene_component";
import Physics from "../physics.js";


export default class AnimationComponent extends SceneComponent {
  onConstruction(animations, defaultAnim) {
    super.onConstruction();

    this.activeAnimation = null;
    this.registeredNotifies = {};

    this.scale = { x: 1, y: 1 };

    if (typeof animations === "object") {
      this.animations = {};

      // Registry notifies
      for (let name in animations) {
        let anim = new Animation(this.world, animations[name]);
        this.animations[name] = anim;

        if (anim instanceof Animation) {
          anim.onNotify(name => {
            let notify = this.registeredNotifies[name];
            if (notify) {
              notify();
            }
          });
        }
      }

      //Select default animation
      if (typeof defaultAnim === "string") {
        this.switchAnimation(defaultAnim);
      }
    }
  }

  switchAnimation(name, callback) {
    let selected = this.animations[name];
    if (selected && this.activeAnimation != selected) {
      selected.reset();
      this.activeAnimation = selected;

      if (typeof callback === "function") {
        this.activeAnimation.onFinishCallback = callback;
      }
    }
  }

  on(name, callback) {
    this.registeredNotifies[name] = callback;
  }

  tick(deltaTime) {
    if (this.activeAnimation) {
      this.activeAnimation.tick(deltaTime);
    }
  }

  render(ctx) {
    if (this.activeAnimation) {
      ctx.save();

      const position = this.worldPosition;
      ctx.translate(position.x * Physics.scale, -position.y * Physics.scale);

      ctx.scale(this.scale.x, this.scale.y);

      this.activeAnimation.render(ctx);

      ctx.restore();
    }
  }
}
AnimationComponent.registry();


/**
* Setup and run an animation
* Use:
* new Animation({
*  img: null,
*  frameWidth: 64,
*  frameHeight: 64,
*  maxFrames: 1,
*  maxHFrames: 1, <- Columns of the sprite sheet
*  initialX: 0,
*  initialY: 0
* });
*/
export class Animation {
  constructor(world, options) {

    this.timePerFrame = 1 / 24;
    this.currentFrametime = 0;
    this.frameCount = 0;
    this.notifies = [];

    options = options || {};

    world.loadImage(options.img, img => {
      this.img = img;
    });

    this.options = {};
    this.options.frameWidth  = options.frameWidth  || 64;
    this.options.frameHeight = options.frameHeight || 64;
    this.options.realWidth   = options.realWidth   || this.options.frameWidth;
    this.options.realHeight  = options.realHeight  || this.options.frameHeight;
    this.options.maxFrames   = options.maxFrames   || 1;
    this.options.maxHFrames  = options.maxHFrames  || 1;
    this.options.initialX    = options.initialX    || 0;
    this.options.initialY    = options.initialY    || 0;
    this.options.loop        = options.loop !== undefined ? options.loop : true;
    this.options.speed       = options.speed       || 1;
    this.notifies            = options.notifies    || [];
  }

  tick(deltaTime) {
    this.currentFrametime += deltaTime;
    if (this.currentFrametime >= this.timePerFrame / Math.abs(this.options.speed)) {
      let animationFrame = this.frameCount % this.options.maxFrames;

      let isLastFrame = animationFrame >= this.options.maxFrames - 1;
      if (this.options.loop || !isLastFrame) {
        ++this.frameCount;
        this.currentFrametime = 0.0;

        //Call notifies
        let notify = this.notifies[animationFrame + 1];
        if (notify && typeof this.notifyCallback == "function") {
          this.notifyCallback(notify);
        }

        if (isLastFrame && typeof this.onFinishCallback === "function") {
          //Animation loop finished
          this.onFinishCallback();
        }
      } else if (typeof this.onFinishCallback === "function") {
        //Animation finished, call and remove the callback
        this.onFinishCallback();
      }
    }
  }

  onNotify(callback) {
    if (typeof callback === "function")
      this.notifyCallback = callback;
  }

  getFramePosition(frame) {
    if (typeof frame !== "number" || this.options == undefined)
      return null;

    //Get animation relative frame
    let animationFrame = frame % this.options.maxFrames;

    if (this.options.speed < 0) {
      //Backwards
      animationFrame = this.options.maxFrames - animationFrame;
    }

    //Calculate column position
    let columnFrame = animationFrame % this.options.maxHFrames;
    //Calculate row position
    let rowFrame = Math.floor(animationFrame / this.options.maxHFrames);

    return {
      x: this.options.initialX + columnFrame * this.options.frameWidth,
      y: this.options.initialY + rowFrame    * this.options.frameHeight
    };
  }

  render(ctx) {
    let position = this.getFramePosition(this.frameCount);
    if (this.img && position) {
      ctx.drawImage(this.img, position.x, position.y,
        this.options.frameWidth, this.options.frameHeight,
        -this.options.realWidth / 2, -this.options.realHeight / 2,
        this.options.realWidth, this.options.realHeight);
    }
  }

  reset() {
    this.frameCount = 0;
    this.onFinishCallback = undefined;
  }
}
