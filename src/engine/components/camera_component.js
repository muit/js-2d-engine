
import Component from "./scene_component";

export default class CameraComponent extends Component {

  beginPlay() {
    super.beginPlay();
    this.world.cameraManager.addCamera(this);
  }

  finishDestruction() {
    super.finishDestruction();

    this.world.cameraManager.removeCamera(this);
  }
}
CameraComponent.registry();
