
import Physics from "./physics";

export default class Renderer {
  constructor(world) {
    this.world = world;
    this.canvas = world.canvas;
    this.ctx = this.canvas.getContext("2d");
  }

  init() {}

  render() {
    // Clean the canvas
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

    // Camera transform: translate
    this.ctx.save();
    let camera = this.world.getActiveCamera();
    if (camera) {
      // Translate canvas with the camera centered
      this.ctx.translate(this.canvas.width / 2 - (camera.position.x * Physics.scale), this.canvas.height / 2 + (camera.position.y * Physics.scale));
    } else {
      this.ctx.translate(0, 0);
    }

    // Debug world origin
    /*this.ctx.beginPath();
    this.ctx.rect(-10, -10, 20, 20);
    this.ctx.fillStyle = "red";
    this.ctx.fill();*/

    // Render Game
    this.renderGame();

    // Camera transform: restore
    this.ctx.restore();

    // Render UI
    this.renderUI();
  }

  renderGame() {
    this.world.scene.render(this.ctx);
    this.world.physics.render(this.ctx);
  }

  renderUI() {
  }
}
