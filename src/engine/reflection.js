
/**
 * Meta programming that adds helpers to js types
 */
export default class Class {
  constructor() {
    this.type = this.constructor;

    this.metadata = this.meta();
    if (typeof (this.metadata) !== "object")
      this.metadata = {};
  }

  isA(Other) {
    return this.type === Other;
  }

  isChildOf(Other) {
    return this.type.isChildOf(Other);
  }

  static isA(Other) {
    // We access the class calling us ->F.isA()
    return Other && this.prototype.constructor === Other;
  }

  static isChildOf(Other) {
    // We access the class calling us ->F.isChildOf()
    return this.isA(Other) || this.prototype.constructor.prototype instanceof Other;
  }


  /**
   * Checks if a class instance is valid
   * @param {Class} instance to check
   * @return true if the instance is valid and inherits from the class that called this
   * B.isValid(c) will be false if C doesn't inherit from B
   */
  static isValid(self) {
    return self instanceof Class && self.isValid();
  }

  /**
   * Checks if a class instance is valid
   * @param {Class} instance to check
   * @return true if the instance is valid
   */
  isValid() { return true; }

  get className() { return this.type.name; }

  get world() { return null; }

  /**
   * Set class's metadata by overriding this.
   */
  meta() { return {}; }
  getMeta(key) { return this.metadata[key]; }

  // Used to find classes by name. Can allow loading scenes
  static registry() {
    if (!Class.all) Class.all = {};
    Class.all[this.name.toLowerCase()] = this;
  }

  static findClass(name) {
    const type = Class.all[name.toLowerCase()];
    console.assert(type, "Did you forget to include '" + name + "' from global_import?");
    return type;
  }
}

