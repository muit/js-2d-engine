/**
 * 2D Transform hierchary library
 * Copyright 2018 - Miguel Fernandez Arce
 */

export class Transform {
  /**
   * Creates a new transform with the following properties:
   * @param {Vector2|Transform} position or copy template
   * @param {number} angle
   * @param {Transform} parent from which this transform is located
   */
  constructor(position, angle, parent) {
    if (position instanceof Transform) {
      // Copy from other transform
      this.localPosition = position.localPosition;
      this.localAngle = angle || position.localAngle;
      this.parent = parent || position.parent;
    } else {
      this.localPosition = position;
      this.localAngle = angle;
      this.parent = parent;
    }
  }

  get worldPosition() {
    if (this.parent) {
      return this.localPosition.copy().rotate(this.parent.worldAngle).add(this.parent.worldPosition);
    } else {
      return this.localPosition.copy();
    }
  }

  set worldPosition(position) {

    if (this.parent) {
      const worldPosition = this.parent.worldPosition;
      const worldAngle = this.parent.worldAngle;

      this.localPosition = new Vector2(position).subtract(worldPosition).rotate(-worldAngle);
    } else {
      this.localPosition = new Vector2(position);
    }
  }

  get worldAngle() {
    return (this.parent ? this.parent.worldAngle : 0) + this.localAngle;
  }

  set worldAngle(angle) {
    if (this.parent) {
      this.localAngle = angle - this.parent.worldAngle;
    } else {
      this.localAngle = angle;
    }
  }
  get localPosition() { return this.__localPosition; }
  set localPosition(position) {
    this.__localPosition = new Vector2(position);
  }

  get localAngle() { return this.__localAngle; }
  set localAngle(angle) {
    this.__localAngle = (typeof (angle) === "number") ? angle : 0;
  }

  transform(position) {
    this.localPosition.add(position);
  }

  rotateDeg(angle) {
    this.__localAngle += angle;
  }

  copy() { return new Transform(this); }
}

export class Vector2 {

  // type safe constructor
  constructor(x, y) {
    const typeX = typeof (x);
    if (!x) {
      this.x = 0;
      this.y = 0;
    } else if (typeX == "object") {
      this.x = x.x || 0;
      this.y = x.y || 0;
    } else {
      // X is a number
      this.x = x;
      this.y = y || 0;
    }
  }

  get toObject() {
    return { x: this.x, y: this.y };
  }

  get toString() { return this.toObject; }

  add(other) {
    if (typeof (other) == "object") {
      this.x += other.x || 0;
      this.y += other.y || 0;
    }
    return this;
  }

  subtract(other) {
    if (typeof (other) == "object") {
      this.x -= other.x || 0;
      this.y -= other.y || 0;
    }
    return this;
  }

  rotateRad(rads) {
    const x = this.x;
    this.x = (this.x * Math.cos(rads)) - (this.y * Math.sin(rads));
    this.y = (x * Math.sin(rads)) + (this.y * Math.cos(rads));

    if (isNearZero(this.x)) this.x = 0;
    if (isNearZero(this.y)) this.y = 0;
    return this;
  }

  rotate(deg) {
    return this.rotateRad(deg2Rad(deg));
  }

  magnitude() { return this.x * this.x + this.y * this.y; }

  copy() { return new Vector2(this); }
}

const radToDeg = 180 / Math.PI;
export function deg2Rad(deg) {
  return deg / radToDeg;
}

export function rad2Deg(rad) {
  return rad * radToDeg;
}

function isNearZero(value) {
  return value < 0.0000000001 && value > -0.0000000001;
}

Math.clamp = function(value, min, max) {
  return Math.min(Math.max(value, min), max);
};

Math.lerp = function(value1, value2, alpha) {
  alpha = alpha > 1 ? 1 : (alpha < 0 ? 0 : alpha);
  return value1 + (value2 - value1) * alpha;
};
