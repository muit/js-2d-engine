// auxiliar code for working with Box2D
// requires jQuery

/* eslint-disable */

import Box2D from "../lib/box-2d";

// Box2D lib
var B2 = {
  Vec2: Box2D.Common.Math.b2Vec2,
  AABB: Box2D.Collision.b2AABB,
  BodyDef: Box2D.Dynamics.b2BodyDef,
  Body: Box2D.Dynamics.b2Body,
  FixtureDef: Box2D.Dynamics.b2FixtureDef,
  Fixture: Box2D.Dynamics.b2Fixture,
  World: Box2D.Dynamics.b2World,
  PolygonShape: Box2D.Collision.Shapes.b2PolygonShape,
  CircleShape: Box2D.Collision.Shapes.b2CircleShape,
  DebugDraw: Box2D.Dynamics.b2DebugDraw,
  MouseJointDef: Box2D.Dynamics.Joints.b2MouseJointDef,
  Shape: Box2D.Collision.Shapes.b2Shape,
  RevoluteJointDef: Box2D.Dynamics.Joints.b2RevoluteJointDef,
  Joint: Box2D.Dynamics.Joints.b2Joint,
  PrismaticJointDef: Box2D.Dynamics.Joints.b2PrismaticJointDef,
  DistanceJointDef: Box2D.Dynamics.Joints.b2DistanceJointDef,
  PulleyJointDef: Box2D.Dynamics.Joints.b2PulleyJointDef,
  ContactListener: Box2D.Dynamics.b2ContactListener
};

export default class Physics {
  constructor(world) {
    this.world = world;
    this.gravity = new B2.Vec2(0, -2);
    this.debug = true;
    this.destructionCache = [];

  }

  // 1 meter = 100 pixels
  static get scale() { return 100; }

  init() {
    var doSleep = false;
    // Create a Box2D world object
    this.physicsWorld = new B2.World(this.gravity, doSleep);

    // DEBUG
    {
      // DebugDraw is used to create the drawing with physics
      var debugDraw = new B2.DebugDraw();
      debugDraw.SetSprite(this.world.renderer.ctx);
      debugDraw.SetDrawScale(Physics.scale);
      debugDraw.SetFillAlpha(0.5);
      debugDraw.SetLineThickness(1.0);
      if (this.debug) {
        debugDraw.SetFlags(B2.DebugDraw.e_shapeBit | B2.DebugDraw.e_jointBit);
      }
      this.physicsWorld.SetDebugDraw(debugDraw);
    }

    // Events
    var listener = B2.ContactListener;
    listener.BeginContact = this.world.onBeginContact;
    listener.EndContact   = this.world.onEndContact;

    listener.PostSolve = function(contact, impulse) {}

    listener.PreSolve = function(contact, oldManifold) {}

    this.physicsWorld.SetContactListener(listener);
  }

  destroyBody(body) {
    this.destructionCache.push(body);
  }

  tick(deltaTime) {
    // Step(timestep , velocity iterations, position iterations)
    this.physicsWorld.Step(deltaTime, 8, 3);
    this.physicsWorld.ClearForces();

    //Destroy bodies
    for(let body of this.destructionCache) {
      this.physicsWorld.DestroyBody(body);
    }
    this.destructionCache = [];
  }

  render(ctx) {
    ctx.save();
    ctx.scale(1, -1);
    this.physicsWorld.DrawDebugData();
    ctx.restore();
  }

  createBox(owner, x, y, width, height, options) {
    // default values
    options = Object.assign({
        'density' : 1.0,
        'friction': 1.0,
        'restitution' : 0.0,

        'linearDamping' : 0.0,
        'angularDamping': 0.0,

        'fixedRotation': false,

        'type' : B2.Body.b2_dynamicBody
    }, options);

    // Fixture: define physics propierties (density, friction, restitution)
    var fix_def = new B2.FixtureDef();

    fix_def.density = options.density;
    fix_def.friction = options.friction;
    fix_def.restitution = options.restitution;

    // Shape: 2d geometry (circle or polygon)
    fix_def.shape = new B2.PolygonShape();

    fix_def.shape.SetAsBox(width, height);

    // Body: position of the object and its type (dynamic, static o kinetic)
    var body_def = new B2.BodyDef();
    body_def.position.Set(x, y);

    body_def.linearDamping = options.linearDamping;
    body_def.angularDamping = options.angularDamping;

    body_def.type = options.type; // b2_dynamicBody
    body_def.fixedRotation = options.fixedRotation;
    body_def.userData = options.user_data;

    var b = this.physicsWorld.CreateBody(body_def);
    var f = b.CreateFixture(fix_def);

    b.SetUserData(owner)
    return b;
  }
}

Physics.Body = B2.Body;
Physics.Vec2 = B2.Vec2;

/* eslint-enable */
