
const WorldState = Object.freeze({
  NOT_STARTED: Symbol("Not Started"),
  RUNNING: Symbol("Running")
});

import Renderer from "./renderer";
import Physics from "./physics";
import GarbageCollector from "./gc";
import { Input } from "./input";
import { CameraManager } from "./cameramanager";
import Scene from "./scene";
import Class from "./reflection";

import "../game/global_import";

const nativeFrameFunction = window.requestAnimationFrame ||
  window.mozRequestAnimationFrame ||
  window.webkitRequestAnimationFrame ||
  window.msRequestAnimationFrame;


export default class World {
  constructor(canvas) {
    this.canvas = canvas;
    this.state = WorldState.NOT_STARTED;
    this.setMaxFPS(60);
    this.preloadPromises = [];

    this.renderer = new Renderer(this);
    this.physics = new Physics(this);
    this.gc = new GarbageCollector(this);
    this.input = new Input(this);
    this.cameraManager = new CameraManager(this);
  }

  init() {
    if (!this.canvas.getContext || this.state !== WorldState.NOT_STARTED) {
      return;
    }

    this.time = 0;
    this.deltaTime = 0;
    this.gameTime = 0;

    this.renderer.init();
    this.physics.init();
    this.gc.init();
    this.input.init();
    this.cameraManager.init();

    this.scene.load();

    this.state = WorldState.RUNNING;

    // Load all assets and then start the game
    Promise.all(this.preloadPromises).then(() => {
      this.scene.beginPlay();
      this.update();
    });
  }

  update() {
    this.requestNextUpdate();

    { // Calculate time
      var now = Date.now();
      this.deltaTime = (now - this.time) / 1000;
      this.time = now;
      this.gameTime += this.deltaTime;

      if (this.deltaTime > 1)
        this.deltaTime = 0;
    }

    this.input.tick(this.deltaTime);

    this.physics.tick(this.deltaTime);

    this.scene.tick(this.deltaTime);

    this.input.postTick();

    this.renderer.render();
  }

  requestNextUpdate() {
    //Keep this context
    let func = () => this.update();

    if (nativeFrameFunction) {
      nativeFrameFunction(func);
    } else {
      window.setTimeout(func, this.frameDuration * 1000);
    }
  }

  setMaxFPS(fps) {
    if (fps > 0) {
      this.frameDuration = 1 / fps;
    }
  }


  // SCENES

  loadScene(SceneType) {
    if (typeof (SceneType) === "string") {
      SceneType = Class.findClass(SceneType);
    }

    if (!this.scene && SceneType.isChildOf(Scene)) {
      this.scene = new SceneType(this);
      this.init();
    }
  }

  // EVENTS
  onBeginContact(contact) { contact; }
  onEndContact(contact) { contact; }

  // HELPERS
  getActiveCamera() {
    return this.cameraManager.getActiveCamera();
  }

  loadImage(src, callback) {
    let promise = new Promise((resolve, reject) => {
      let img = new Image();
      img.src = src;
      img.onload = () => {
        resolve(img);
        if (callback)
          callback(img);
      };

      img.onerror = err => {
        reject(err);
      };
    });

    if (this.state == WorldState.NOT_STARTED) {
      this.preloadPromises.push(promise);
    }
  }
}
