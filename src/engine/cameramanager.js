
export class Camera {
  constructor(component) {
    this.component = component;
  }

  get position() {
    return this.component.localPosition;
  }
}

// Camera Manager
export class CameraManager {
  constructor(world) {
    this.world = world;
    this.activeCamera = null;
    this.cameras = new Map();
  }

  init() {
  }

  addCamera(cameraComponent) {
    if (this.cameras.has(cameraComponent)) {
      return null;
    }

    let camera = new Camera(cameraComponent);
    this.cameras.set(cameraComponent, camera);

    if (!this.getActiveCamera()) {
      this.setActiveCamera(camera);
    }
    return camera;
  }

  getCamera(cameraComponent) {
    return this.cameras.get(cameraComponent);
  }

  getActiveCamera() {
    return this.activeCamera;
  }

  removeCamera(cameraComponent) {
    let wasActiveCamera = this.getCamera(cameraComponent) === this.activeCamera;
    if (this.cameras.delete(cameraComponent)) {
      // Set first camera active if removed one was active
      if (wasActiveCamera) {
        this.setActiveCamera(this.values().next().value);
      }
      return true;
    }
    return false;
  }

  setActiveCamera(camera) {
    if (!camera || camera instanceof Camera) {
      this.activeCamera = camera;
    } else {
      this.activeCamera = this.getCamera(camera);
    }
  }
}
