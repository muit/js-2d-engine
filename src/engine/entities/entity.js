
import Class from "../reflection";
import Scene from "../scene";
import Component from "../components/component";
import SceneComponent from "../components/scene_component";

const EntityLifeState = Object.freeze({
  Valid: Symbol("Valid"),
  Pending: Symbol("Pending Destruction"),
  Destroyed: Symbol("Destroyed")
});

export default class Entity extends Class {
  constructor(scene) {
    super();
    this.scene = scene;
    this.components = [];

    /** Setup all components */
    let data = this.getJsonData();

    // Create all entities
    if (data && typeof (data) == "object") {
      if (data.root) {
        this.root = this.createComponentFromParentData(data.root);
      }
    }
  }

  preConstruction(position, angle) {
    // Ensure root exists and assign it the entity's transform
    if (!(this.root instanceof SceneComponent)) {
      this.root = this.createComponent(SceneComponent);
    }
    this.worldPosition = position;
    this.worldAngle = angle;
  }

  onConstruction() {}

  beginPlay() {
    this.iterateComponents(comp => comp.beginPlay());
  }

  tick(deltaTime) {
    this.iterateComponents(comp => comp.tick(deltaTime));
  }

  render(ctx) {
    this.iterateComponents(comp => {
      if (comp.isChildOf(SceneComponent)) {
        comp.render(ctx);
      }
    });
  }

  /** Called just after the entity gets marked for destruction */
  beginDestruction() {
    this.validState = EntityLifeState.Pending;
    this.iterateComponents(comp => comp.beginDestruction());
  }

  /** Called just before the entity gets destroyed, at the end of the frame */
  finishDestruction() {
    this.validState = EntityLifeState.Destroyed;
    this.iterateComponents(comp => comp.finishDestruction());
  }

  /**
   * Creates a component on this entity
   * @param {Component} Type of the component
   * @param {*} args that will go into "onConstruction(args)"
   * @return {Component} created component
   */
  createComponent(Type, ...args) {
    if (typeof (Type) === "string") {
      Type = Class.findClass(Type);
    }

    if (Type && Type.isChildOf(Component)) {
      return Type.create(this, ...args);
    }
    return null;
  }

  findComponentOfType(Type) {
    if (typeof (Type) === "string") {
      Type = Class.findClass(Type);
    }

    if (Type) {
      for (let comp of this.components) {
        if (comp && comp.isChildOf(Type))
          return comp;
      }
    }
    return null;
  }

  /**
   * Execute a callback for each component of this entity
   * @param {function(Component)} callback to call
   */
  iterateComponents(callback) {
    if (typeof (callback) != "function")
      return;

    for (let comp of this.components) {
      callback(comp);
    }
  }

  createComponentFromParentData(data, parent) {
    if (typeof (data.type) === "string") {
      data.type = Class.findClass(data.type);
    }


    if (data.type.isChildOf(SceneComponent)) {
      let comp;
      if (data.parameters instanceof Array) {
        comp = this.createComponent(data.type, parent, ...data.parameters);
      } else if (data.parameters) {
        comp = this.createComponent(data.type, parent, data.parameters);
      } else {
        comp = this.createComponent(data.type, parent);
      }

      if (data.children instanceof Array) {
        for (let childComp of data.children) {
          this.createComponentFromParentData(childComp, comp);
        }
      }

      return comp;
    }

    if (data.parameters instanceof Array)
      return this.createComponent(data.type, ...data.parameters);
    else
      return this.createComponent(data.type);
  }

  isValid() {
    return this.scene instanceof Scene && this.validState !== EntityLifeState.Valid;
  }

  getJsonData() {
    return null;
  }

  /**
   * Creates a component on this entity
   * @param {Component} Type of the component
   * @param {Vector2} position to spawn at
   * @param {number} angle to spawn with
   * @param {*} args that will go into "onConstruction(args)"
   * @return {Entity} created entity
   */
  static spawn(scene, position, angle, ...args) {
    if (Scene.isValid(scene)) {
      let entity = new this(scene);

      // registry this entity on the scene
      scene.entities.push(entity);

      entity.preConstruction(position, angle);
      entity.onConstruction(...args);

      if (scene.hasBegunPlay)
        entity.beginPlay();
      return entity;
    }
    return null;
  }

  /** Called when this entity have to be destroyed */
  despawn() {
    if (this.isValid() || !this.scene) {
      if (this.scene) {
        this.scene.entities.pop(this);
      }
      this.scene.gc.destroy(this);
    }
  }

  get world() { return this.scene.world; }

  get worldPosition() { return this.root.transform.localPosition; }
  get worldAngle() { return this.root.transform.localAngle; }
  set worldPosition(position) {
    this.root.transform.localPosition = position;
  }
  set worldAngle(angle) { this.root.transform.localAngle = angle; }
}
Entity.registry();
