
import Entity from "./entities/entity";

export default class GarbageCollector {
  constructor(world) {
    this.world = world;
    this.destructionCache = [];
  }

  init() {}

  destroy(object) {
    if (object instanceof Entity) {
      object.beginDestruction();
      this.destructionCache.push(object);
    }
  }

  clean() {
    for (let object of this.destructionCache) {
      object.finishDestruction();
    }
    this.destructionCache = [];
  }
}
