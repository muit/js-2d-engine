// key events

const KeyCode = Object.freeze({
  KEY_LEFT: 37,
  KEY_UP: 38,
  KEY_RIGHT: 39,
  KEY_DOWN: 40,
  KEY_A: 65,
  KEY_W: 87,
  KEY_D: 68,
  KEY_S: 83,
  KEY_PAUSE: 19,
  KEY_SPACE: 32
});

class Input {
  constructor(world) {
    this.world = world;
    this.mouse = { x: 0, y: 0 };
    this.keymap = {
      keyup: {},
      keypressed: {}
    };
  }

  init() {
    let canvas = this.world.canvas;

    // Mouse
    canvas.addEventListener("mousedown", event => {
      var rect = this.world.canvas.getBoundingClientRect();
      this.mouse.x = event.clientX - rect.left;
      this.mouse.y = event.clientY - rect.top;
    }, false);

    canvas.addEventListener("mousemove", event => {
      var rect = this.world.canvas.getBoundingClientRect();
      this.mouse.x = event.clientX - rect.left;
      this.mouse.y = event.clientY - rect.top;
    }, false);

    addEvent(document, "keydown", e => {
      this.keymap[e.keyCode] = true;
      this.keymap.keypressed[e.keyCode] = true;
    });

    addEvent(document, "keyup", e => {
      this.keymap.keyup[e.keyCode] = true;
      this.keymap[e.keyCode] = false;
    });
  }

  isKeyPressed(keycode) {
    return this.keymap[keycode];
  }

  isKeyDown(keycode) {
    return this.keymap.keypressed[keycode] !== undefined;
  }

  isKeyUp(keycode) {
    return this.keymap.keyup[keycode];
  }

  tick(deltaTime) { deltaTime; }

  postTick() {
    this.keymap.keyup = {};
    this.keymap.keypressed = {};
  }
}

function addEvent(element, eventName, func) {
  if (element.addEventListener)
    element.addEventListener(eventName, func, false);
  else if (element.attachEvent)
    element.attachEvent(eventName, func);
}

export { Input, KeyCode };
