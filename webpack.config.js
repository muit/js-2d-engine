var webpack = require("webpack");
var path = require("path");

/* eslint-disable no-undef */
var environment = process.env["NODE_ENV"] || "development";

var soundPath = pathForRegexp("src/assets/sounds");
var soundRegExp = new RegExp(soundPath + ".*.(mp3|ogg|wav)$", "i");

var imagePath = pathForRegexp("src/assets/images");
var imageRegExp = new RegExp(imagePath + ".*.(jpe?g|png|gif|svg)$", "i");

var fontPath = pathForRegexp("src/assets/fonts");
var fontRegExp = new RegExp(fontPath + ".*.(eot|svg|ttf|woff2?)$", "i");

var htmlPath = pathForRegexp("src/");
var htmlRegExp = new RegExp(htmlPath + ".*.html", "i");

function escapeForRegExp(s) {
  return s.replace(/[-/\\^$*+?.()|[\]{}]/g, "\\$&");
}

function pathForRegexp(url) {
  var normalized = path.resolve(__dirname, url);
  return escapeForRegExp(normalized);
}

module.exports = {
  mode: "development",
  entry: {
    index: [ "./src/main" ]
  },
  devtool: "source-map",
  output: {
    path: __dirname + "/build/html",
    filename: "[name].js"
  },
  module: {
    rules: [
      {
        enforce: "pre",
        test: soundRegExp,
        loader: "file-loader?hash=sha512&digest=hex&name=[path][name].[ext]&context=src"
      },
      {
        enforce: "pre",
        test: imageRegExp,
        loader: "file-loader",
        options: {
          name: "[path][name].[ext]",
          hash: "sha512",
          digest: "hex"
        }
      },
      {
        enforce: "pre",
        test: fontRegExp,
        loader: "file-loader?hash=sha512&digest=hex&name=[path][name].[ext]&context=src"
      },
      {
        enforce: "pre",
        test: htmlRegExp,
        loader: "file-loader?hash=sha512&digest=hex&name=[path][name].[ext]&context=src"
      },
      {
        test: /\.js$/,
        include: path.resolve(__dirname, "src"),
        exclude: /(node_modules|.cache)/,
        loader: "babel-loader"
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      __PRODUCTION__: environment === "production",
      __TEST__: environment === "test",
      __DEVELOPMENT__: environment === "development"
    })
  ]
};
