# PipeJS
A game engine library using canvas and box2d that exports to Web, PC and Mobile.

# Get started

1. Fork this repo & clone it to your computer
2. Install [Node.js](https://nodejs.org)
3. Run `npm install` inside the project folder
4. Run `npm start` inside the project folder
5. Navigate to [localhost:4000](localhost:4000) in your browser


# Sharing your game

1. Run `npm run build` inside the project folder
2. Zip and distribute build folder - or - Rename, upload, and serve build directory from your website
